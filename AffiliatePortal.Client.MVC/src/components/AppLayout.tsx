﻿import * as React from "react";

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AppBar from 'material-ui/AppBar';
import { FontIcon, GridList, Paper, Divider } from "material-ui";
import { colors } from "material-ui/styles";
import { LeftMenu } from "./LeftMenu";


export interface AppLayoutProps { }



export class AppLayout extends React.Component<AppLayoutProps, undefined> {

    constructor(props) {
        super(props);
    }



    handleToggle = () => { alert('test'); this.setState({ open: true });  }
    handleClose = () => this.setState({ open: false });

    render() {


        return <div>

            <div>
                <AppBar onLeftIconButtonTouchTap={this.handleToggle} title="Affiliate portal" />
                <LeftMenu />
            </div>

        </div>
    }
}