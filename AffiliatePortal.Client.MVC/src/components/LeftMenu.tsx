﻿import * as React from "react"
import { Drawer, FontIcon, Paper, Divider } from "material-ui";
import MenuItem from 'material-ui/MenuItem';
import Menu from 'material-ui/Menu';

import { colors } from "material-ui/styles";

export interface LeftMenuState { open: boolean }
export interface LeftMenuProps { }
const style = {
    paper: {
        display: 'inline-block',
        float: 'left',
        width: '100%'

    },
    rightIcon: {
        textAlign: 'center',
        lineHeight: '24px',
    }
};
export class LeftMenu extends React.Component<LeftMenuProps, LeftMenuState>{

    constructor(props) {
        super(props);
        this.state = { open: true }
    }

    render() {
        return <Drawer
            containerStyle={{ height: 'calc(100% - 64px)', top: 64 }}
            docked={true}
            width={300}
            open={this.state.open}
            onRequestChange={(open) => this.setState({ open })} >
            <div className="center" style={{ margin: '30px' }}>
                <FontIcon className="material-icons" style={{ fontSize: 70, alignSelf: "center" }} size={280} color={colors.blueGrey700}>account_circle</FontIcon>
                <h3>Welcome User!</h3>
            </div>

            <Paper style={style.paper}>
                <Menu style={{ width: 270 }} >
                    <MenuItem primaryText="Dashboard" style={{ color: colors.blue800 }} rightIcon={<FontIcon style={{ color: colors.blue900 }} className="material-icons">show_chart</FontIcon>} />
                    <MenuItem primaryText="Website" style={{ color: colors.blue800 }} rightIcon={<FontIcon style={{ color: colors.blue900 }} className="material-icons">home</FontIcon>} />

                    <Divider />

                    <MenuItem primaryText="Marketing Materials" style={{ color: colors.blue800 }} rightIcon={<FontIcon style={{ color: colors.blue400 }} className="material-icons">photo_size_select_actual</FontIcon>} />
                    <MenuItem primaryText="Competition" style={{ color: colors.blue800 }} rightIcon={<FontIcon style={{ color: colors.blue400 }} className="material-icons">supervisor_account</FontIcon>} />

                </Menu>
            </Paper>
        </Drawer>
    }
}