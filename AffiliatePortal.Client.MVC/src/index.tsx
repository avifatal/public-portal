﻿import * as React from "react";
import * as ReactDOM from "react-dom";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { AppLayout } from "./components/AppLayout";
import { getMuiTheme, colors } from "material-ui/styles";
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import { blueGrey50 } from "material-ui/styles/colors";
import * as injectTapEventPlugin from 'react-tap-event-plugin';

 

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
// Used by Material-ui


const theme = getMuiTheme({
    
    palette: {
        primary1Color: colors.blueGrey500,
        primary2Color: colors.blueGrey700,
        primary3Color: colors.blueGrey400,
        accent1Color: colors.blueGrey200,
        accent2Color: colors.blueGrey100,
        accent3Color: colors.blueGrey500
    },
    
});

injectTapEventPlugin();
ReactDOM.render(
    <MuiThemeProvider muiTheme={theme} ><AppLayout /></MuiThemeProvider>,
    document.getElementById('app')
);