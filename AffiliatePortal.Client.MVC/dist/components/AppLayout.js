"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var AppBar_1 = require("material-ui/AppBar");
var LeftMenu_1 = require("./LeftMenu");
var AppLayout = (function (_super) {
    __extends(AppLayout, _super);
    function AppLayout(props) {
        var _this = _super.call(this, props) || this;
        _this.handleToggle = function () { alert('test'); _this.setState({ open: true }); };
        _this.handleClose = function () { return _this.setState({ open: false }); };
        return _this;
    }
    AppLayout.prototype.render = function () {
        return React.createElement("div", null,
            React.createElement("div", null,
                React.createElement(AppBar_1.default, { onLeftIconButtonTouchTap: this.handleToggle, title: "Affiliate portal" }),
                React.createElement(LeftMenu_1.LeftMenu, null)));
    };
    return AppLayout;
}(React.Component));
exports.AppLayout = AppLayout;
//# sourceMappingURL=AppLayout.js.map