"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var material_ui_1 = require("material-ui");
var MenuItem_1 = require("material-ui/MenuItem");
var Menu_1 = require("material-ui/Menu");
var styles_1 = require("material-ui/styles");
var style = {
    paper: {
        display: 'inline-block',
        float: 'left',
        width: '100%'
    },
    rightIcon: {
        textAlign: 'center',
        lineHeight: '24px',
    }
};
var LeftMenu = (function (_super) {
    __extends(LeftMenu, _super);
    function LeftMenu(props) {
        var _this = _super.call(this, props) || this;
        _this.state = { open: true };
        return _this;
    }
    LeftMenu.prototype.render = function () {
        var _this = this;
        return React.createElement(material_ui_1.Drawer, { containerStyle: { height: 'calc(100% - 64px)', top: 64 }, docked: true, width: 300, open: this.state.open, onRequestChange: function (open) { return _this.setState({ open: open }); } },
            React.createElement("div", { className: "center", style: { margin: '30px' } },
                React.createElement(material_ui_1.FontIcon, { className: "material-icons", style: { fontSize: 70, alignSelf: "center" }, size: 280, color: styles_1.colors.blueGrey700 }, "account_circle"),
                React.createElement("h3", null, "Welcome Avi Fatal!")),
            React.createElement(material_ui_1.Paper, { style: style.paper },
                React.createElement(Menu_1.default, { style: { width: 270 } },
                    React.createElement(MenuItem_1.default, { primaryText: "Dashboard", style: { color: styles_1.colors.blue800 }, rightIcon: React.createElement(material_ui_1.FontIcon, { style: { color: styles_1.colors.blue900 }, className: "material-icons" }, "show_chart") }),
                    React.createElement(MenuItem_1.default, { primaryText: "Website", style: { color: styles_1.colors.blue800 }, rightIcon: React.createElement(material_ui_1.FontIcon, { style: { color: styles_1.colors.blue900 }, className: "material-icons" }, "home") }),
                    React.createElement(material_ui_1.Divider, null),
                    React.createElement(MenuItem_1.default, { primaryText: "Marketing Materials", style: { color: styles_1.colors.blue800 }, rightIcon: React.createElement(material_ui_1.FontIcon, { style: { color: styles_1.colors.blue400 }, className: "material-icons" }, "photo_size_select_actual") }),
                    React.createElement(MenuItem_1.default, { primaryText: "Competition", style: { color: styles_1.colors.blue800 }, rightIcon: React.createElement(material_ui_1.FontIcon, { style: { color: styles_1.colors.blue400 }, className: "material-icons" }, "supervisor_account") }))));
    };
    return LeftMenu;
}(React.Component));
exports.LeftMenu = LeftMenu;
//# sourceMappingURL=LeftMenu.js.map