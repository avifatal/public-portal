"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var MuiThemeProvider_1 = require("material-ui/styles/MuiThemeProvider");
var AppLayout_1 = require("./components/AppLayout");
var styles_1 = require("material-ui/styles");
var injectTapEventPlugin = require("react-tap-event-plugin");
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
// Used by Material-ui
var theme = styles_1.getMuiTheme({
    palette: {
        primary1Color: styles_1.colors.blueGrey500,
        primary2Color: styles_1.colors.blueGrey700,
        primary3Color: styles_1.colors.blueGrey400,
        accent1Color: styles_1.colors.blueGrey200,
        accent2Color: styles_1.colors.blueGrey100,
        accent3Color: styles_1.colors.blueGrey500
    },
});
injectTapEventPlugin();
ReactDOM.render(React.createElement(MuiThemeProvider_1.default, { muiTheme: theme },
    React.createElement(AppLayout_1.AppLayout, null)), document.getElementById('app'));
//# sourceMappingURL=index.js.map